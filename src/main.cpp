#include <libpq-fe.h>
#include <iostream>
#include <cstring>

using namespace std;

void Recup_Donnees(PGconn *);
unsigned int LongueurChampsPlusLong(PGresult *);

int main()
{
  int code_retour;
  char donnees_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=h.moustaine user=h.moustaine password=P@ssword";
  PGPing code_retour_ping;
  PGconn *connexion;
  code_retour = 0;
  code_retour_ping = PQping(donnees_connexion);

  if(code_retour_ping == PQPING_OK)
  {
    cout << "Le serveur est accessible" << endl;
    connexion = PQconnectdb(donnees_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de donnée '" << PQhost(connexion) << "' a été établie avec les paramètres suivants :" << endl;
      cout << "* utilisateur : " << PQuser(connexion) << endl;
      cout << "* mot de passe : " ;
      char  mdp = strlen(PQpass(connexion));

      for(int i = 0; i < mdp; i++)
      {
        cout << "*";
      }

      cout << endl;
      cout << "* base de donnée : " << PQdb(connexion) << endl;
      cout << "* port TCP : " << PQport(connexion) << endl;
      int chiffrement_ssl = PQsslInUse(connexion);

      if(chiffrement_ssl  == 1)
      {
        cout << "* chiffrement SSL : true" << endl;
      }
      else
      {
        cout << "* chiffrement SSL : false" << endl;
      }

      cout << "* encodage : " << PQparameterStatus(connexion, "server_encoding") << endl;
      cout << "* version du protocole : " << PQprotocolVersion(connexion) << endl;
      cout << "* version du serveur : " << PQserverVersion(connexion) << endl;
      cout << "* version de la bibliotèque : " << PQlibVersion() << endl;
      Recup_Donnees(connexion);
    }//fermeture du if PQstatus
    else
    {
      cerr << " La connexion n'a pas pu être établie" << endl;
      code_retour = 1;
    }
  }//fermeture du if code_retour_ping
  else
  {
    cerr << "Le serveur ne semble pas être accessible. Veuillez contacter votre administrateur système" << endl;
    code_retour = 1;
  }

  return code_retour;
}

unsigned int LongueurChampsPlusLong(PGresult * resultat)
{
  unsigned int maximum = 0;
  unsigned int nbChamps = 0;
  nbChamps = PQnfields(resultat);

  for(unsigned int numChamps = 0; numChamps < nbChamps; ++numChamps)
  {
    unsigned int nbCharacters = strlen(PQfname(resultat, numChamps));
    if(nbCharacters > maximum){
      maximum = nbCharacters;
    }
  }
  return maximum;
}

void Recup_Donnees(PGconn *connexion)
{
  int nom, ligne, info, NombColonnes;
  PGresult *res = PQexec(connexion, "SET SCHEMA 'si6'; SELECT *	FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");

  if(PQresultStatus(res) == PGRES_TUPLES_OK)
  {
    cout << "\tPGRES_TUPLES_OK" << endl;
    NombColonnes = PQnfields(res);

    for(nom = 0; nom < NombColonnes; nom++)
    {
      cout << " | " << PQfname(res, nom);
    }

    cout << "\n" << endl;

    for(ligne = 0; ligne < PQntuples(res); ligne++)
    {
      for(info = 0; info < NombColonnes; info++)
      {
        cout << "|" << PQgetvalue(res, ligne, info);
      }

      cout << "\n" << endl;
    }

    for(unsigned int etoile = 0; etoile < LongueurChampsPlusLong(res); ++etoile){
      cout << "*";
    }
    cout << endl;


    PQclear(res);
  }
  else
  {
    cerr << "ERREUR LA BASE DE DONNE N'À PAS PU ETRE CHARGÉ" << endl;
    PQclear(res);
  }
}

